
from django.http import HttpResponse
from .models import Products, Categories
from django.core import serializers
from django.template import loader


# прикрути асинхронку
# прикрути пагинацию (how to paginate data in Django and display it using HTML)

def index(request):
    
    all_categories = Categories.objects.order_by("id")
    template = loader.get_template("catalogs/my_product.html")

    all_ob = Products.objects.order_by("category_id")

    context = {
        "categories" : all_categories,
        "products" : all_ob
    }
    return HttpResponse(template.render(context, request))

def category_page(request, category_id):

    products_in_category = Products.objects.filter(category=category_id)

    template = loader.get_template("catalogs/category.html")
    context = {
        "products" : products_in_category,
    }

    return HttpResponse(template.render(context, request))

def product_page(request, product_id):
    Product = Products.objects.get(id=product_id)

    template = loader.get_template("catalogs/product.html")
    context = {
        "product" : Product,
    }

    return HttpResponse(template.render(context, request))
