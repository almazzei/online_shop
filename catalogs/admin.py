from django.contrib import admin
from .models import Files, Categories, Products

class FilesAdmin(admin.ModelAdmin):
    pass


class CategoriesAdmin(admin.ModelAdmin):
    def save_object(self, request, obj, form, change):
        file_obj = Files.objects.create(
            file=form.cleaned_data['product_image'], 
            name=form.cleaned_data['title'])
        obj.product_image = file_obj
        super().save_object(request, obj, form, change)

class ProductsAdmin(admin.ModelAdmin):
    def save_object(self, request, obj, form, change):
        file_obj = Files.objects.create(
            file=form.cleaned_data['product_image'], 
            name=form.cleaned_data['title']
            )
        obj.product_image = file_obj
        super().save_object(request, obj, form, change)

admin.site.register(Files, FilesAdmin)
admin.site.register(Categories, CategoriesAdmin)
admin.site.register(Products, ProductsAdmin)