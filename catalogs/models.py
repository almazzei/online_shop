# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

class Files(models.Model):
    id = models.AutoField(primary_key=True)
    file = models.FileField(upload_to='files/')
    name = models.CharField(max_length=255, null=True, blank=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'files'

class Categories(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100, blank=True, null=True)
    category_image = models.ForeignKey(Files, models.DO_NOTHING, blank=True, null=True)
    
    def __str__(self):
        return self.title

    class Meta:
        db_table = 'categories'


class Products(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    volume = models.IntegerField(blank=True, null=True)
    product_image = models.ForeignKey(Files, models.DO_NOTHING, blank=True, null=True)
    category = models.ForeignKey(Categories, models.DO_NOTHING, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self): 
        return self.title

    class Meta:
        db_table = 'products'