from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("categories/<int:category_id>/", views.category_page, name="category_page"),
    path("products/<int:product_id>/", views.product_page, name="product_page"),
]
