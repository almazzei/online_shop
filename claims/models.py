# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

class Claims(models.Model):
    id = models.IntegerField(primary_key=True)
    buyer_email = models.CharField(max_length=50, blank=True, null=True)
    buyer_phone = models.CharField(max_length=20, blank=True, null=True)
    item_id = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        db_table = 'claims'


# class Items(models.Model):
#     id = models.IntegerField(primary_key=True)
#     title = models.CharField(max_length=100)
#     price = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
#     volume = models.IntegerField(blank=True, null=True)
#     image_path = models.CharField(max_length=255, blank=True, null=True)
#     category = models.ForeignKey(Categories, models.DO_NOTHING, blank=True, null=True)

#     def __str__(self): # __str__ for Python 3, __unicode__ for Python 2
#         return self.title

#     class Meta:
#         db_table = 'items'
