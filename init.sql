'''Items
 id int
 title string
 price decimal
 volume int
 image_path string
 category_id int [FK]
Claims
 id int
 email_buyer string
 phone_buyer string
 item_id [array]
Category
 id int
 name string
 image_path string'''

create table categories (
    id int primary key,
    title varchar(100),
    image_path varchar(255)
);


create table claims (
    id int primary key,
    buyer_email varchar(50),
    buyer_phone char(20),
    item_id integer[]
);

create table items (
    id int primary key,
    title varchar(100) not null,
    price decimal,
    volume int,
    image_path varchar(255),
    category_id int,
    constraint fk_category
        foreign key(category_id)
            references categories(id)    
);